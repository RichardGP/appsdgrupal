const mqtt = require('mqtt');
const express = require('express');
const socket = require('socket.io');
const http = require('http');

const client = mqtt.connect('mqtt://localhost');

const app = express();
const server = http.createServer(app);
const io = socket.listen(server);
const connection = require('./connection/getConnection.js');

client.on('connect', ()=>{
    client.subscribe('nodeMCUMQ5',(err)=>{
        if(!err){
            client.on('message', (topic, message)=>{
                try{
                    guardar(message);
                }catch(errorGuardar){
                    console.log(errorGuardar);
                }
            });
        }
    });
});

io.on('connect', (socket)=>{
    console.log('Nuevo socket conectado');
    socket.on('historicoSend', (fecha)=>{
        console.log(fecha);
        connection((err, conn)=>{
            //let sql = `SELECT * FROM mq5 where timeSensor like '${fecha}%' order by timeSensor asc`;
            let sql = `select max(valueSensor) as valueSensor, timeSensor from nodemcu.mq5 where timeSensor like '${fecha}%' group by hour(timeSensor)`;
            conn.query(sql, (err, data)=>{
                if(!err){
                    let valores = JSON.parse(JSON.stringify(data));
                    console.log(valores);                
                    io.emit('Response', valores);
                    console.log('dato enviado');
                }else{
                    throw err;   
                }
            });
        });
    });
});


app.use(express.static(__dirname+'/public'));

app.get('/', (req, res)=>{
    res.sendFile(__dirname+'/public/index.html');

});

function consultar(){
    connection((err, conn)=>{
        if(!err){
            conn.query('SELECT * FROM mq5 order by timeSensor desc limit 1', (err,gas)=>{
                if(err){
                    throw err;
                }else{
                    let valores = JSON.parse(JSON.stringify(gas));
                    console.log(valores);
                    for(let valor in valores){
                        io.emit('sensorNode',valores[valor]);
                    }
                    console.log('Datos enviados');
                }
            });
        }
    });
}


function guardar(message){
    console.log('Mensaje recibido: ' + message.toString());
                
    let msg = message.toString().split("$");

    //'2030-12-2 23:59:59'
    let fecha = msg[0];
    let valor = msg[1];
    let voltaje = msg[2];               

    connection((err, conn)=>{
        if(!err){
            let sql = `INSERT INTO mq5(valueSensor, timeSensor, voltajeSensor) VALUES(${valor},'${fecha}', ${voltaje});`;
            conn.query(sql, ((err, res)=>{
                if(!err){
                    console.log('registro guardado');
                    consultar();
                }else{
                    console.log(err);
                    return err;                    
                }
            }));
            //io.emit('nuevo', 'nuevo');
            
        }
    });     
}

server.listen(3000,()=>{
    console.log('Server on port ',3000);
});